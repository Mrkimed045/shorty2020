# Shorty2020

Shorty is a web application that generates the shorter URL for the URL provided by user. 
When someone tries to open generated short version of URL it is resolved, and web client is redirected to the original URL. 
Application is architecturally divided into the two main sections, administration and redirection. 
All web APIs are REST services with JSON formatting of the data.

## Features

* Spring web
* Spring data JPA
* H2 In-memory Database
* Spring security
* Springfox Swagger2 API documantation

## How to build and run Shorty2002
To build your application locate yourself to the project folder, and run the following command:
```batch
mvnw clean install
```
This command will run all the tests before building your app. If you want to build your app withuot running tests
 use the command below:
 ```batch
mvnw clean install -DskipTest
 ```

You can run Shorty2020 from your IDE, or from Command Line:
Locate to your project folder, and run the following command:

```batch
java -jar target\shorty2-0.0.1-SNAPSHOT.jar
```
Keep in mind that this application uses port 8080 and if the port is in use already, this application won't work.

## Usage with Postman

### Four end-points:

#### 1. Registration

Allows simple user registration via POST request and with following URL:
```
http://localhost:8080/administration/register
```
In the request body give account ID in JSON format as following:
```json
{
  "accountID": "marco"
}
```
If account with its ID already exists in database, you will receive message(success: false).
If not you will get new generated password that you can use later on. 


#### 2. Url Shortening

Once you are registered you can use your username and password to short URLs.
Shortening is done through POST request with following URL:
```
http://localhost:8080/administration/short
```
In Authentication header choose option Basic Auth and provide your username and password you received upon registration.

In request body give the url you want to shorten as following:
```json
{
  "url": "https://gitlab.com/Mrkimed045/shorty2020",
  "redirectType": 302       //OPTIONAL default is 302, you can choose 302 or 301
}
```
If username and password are valid you will receive shortened URL for response.

#### 3. Statistics

You can use statistics end-point to display how many times did every short URL redirect us to the full URL page.
This one is a GET request with following URL:
```
http://localhost:8080/administration/statistics
```
In Authentication header choose option Basic Auth and provide your username and password, just like you did with shortening.

#### 4. Redirection

All that is left is to try redirection. 
Use the short Url that was generated for your full URL and you will get redirected to the original web page.
How many time we were redirected is stored and we can display it with statistics end-point above.

Make sure you are authenticated in header for redirection!
This one is GET request and url looks something like this:
```
http://localhost:8080/shorty/{shortUrlpart}
```
---

## H2-console

You can access your H2 database console from your browser:
```
http://localhost:8080/h2-console
```
When you try to access h2-console from your browser, you will be asked to provide username and password:
 * username = admin 
 * password = admin
 
Once you register you will be directed to this page:

![h2-console](https://www.onlinetutorialspoint.com/wp-content/uploads/2018/02/Spring-Boot-H2-Database-1-min.png)

Here don't change any information given, just click connect to enter the console.

 
 ## Swagger2 API documentation
 
 You can access generated Swagger2 UI documentation in your browser:
 ```
 http://localhost:8080/swagger-ui.html
 ```


