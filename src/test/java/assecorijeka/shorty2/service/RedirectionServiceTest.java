package assecorijeka.shorty2.service;

import assecorijeka.shorty2.dao.facade.ShortUrlDaoFacade;
import assecorijeka.shorty2.service.model.ShortUrlServiceModel;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.view.RedirectView;

import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class RedirectionServiceTest {

    @Mock
    private ShortUrlDaoFacade shortUrlDaoFacade;

    @InjectMocks
    private RedirectionService redirectionService;

    @Test
    void redirect_WithExistingShortUrl(){
        //arrange
        String shortUrl = "abcdefg";
        ShortUrlServiceModel existingShortUrl = new ShortUrlServiceModel("https://www.facebook.com/", 302, "marco", "abcdefg", 0);
        when(shortUrlDaoFacade.findById(shortUrl)).thenReturn(Optional.of(existingShortUrl));

        //act
        RedirectView redirectView = redirectionService.redirect(shortUrl);

        //assert
        assertTrue(redirectView.isRedirectView());
        assertEquals("https://www.facebook.com/", redirectView.getUrl());
    }

    @Test
    void redirect_WithNonExistingShortUrl(){
        //arrange
        String shortUrl = "abcdefg";
        Optional<ShortUrlServiceModel> nonExistingShortUrl = Optional.empty();
        when(shortUrlDaoFacade.findById(shortUrl)).thenReturn(nonExistingShortUrl);

        //act
        Throwable exception =  assertThrows(ResponseStatusException.class, () -> redirectionService.redirect(shortUrl));

        //assert
        assertEquals("404 NOT_FOUND \"Page does not exist in database!\"", exception.getMessage());

    }

}
