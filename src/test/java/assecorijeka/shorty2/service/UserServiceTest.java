package assecorijeka.shorty2.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import assecorijeka.shorty2.dao.facade.UserDaoFacade;
import assecorijeka.shorty2.service.model.UserServiceModel;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

@ExtendWith(SpringExtension.class)
class UserServiceTest{
	
	@Mock
	private UserDaoFacade userDaoFacade;

	@InjectMocks
	private UserService userService;

	@Test
	void saveUser_whenIdExists() {
		// arrange
		UserServiceModel userServiceModel = new UserServiceModel("duplicate", null);
		when(userDaoFacade.existsById(userServiceModel.getAccountId())).thenReturn(true);
		
		// act
		Optional<UserServiceModel> returnModel = userService.saveUser(java.util.Optional.of(userServiceModel));
		
		// assert
		assertFalse((returnModel.isPresent()));
	}
	
	@Test
	void saveUser_whenIdDoesNotExists() throws Exception {
		// arrange
		UserServiceModel savingInput = new UserServiceModel("unique", null);
		when(userDaoFacade.existsById(savingInput.getAccountId())).thenReturn(false);
		when(userDaoFacade.save(Optional.of(savingInput))).thenReturn(Optional.of(savingInput));

		// act
		Optional<UserServiceModel> returnModel = userService.saveUser(java.util.Optional.of(savingInput));

		// assert
		assertTrue(returnModel.isPresent());
		assertEquals("unique", returnModel.get().getAccountId());
		assertNotNull(returnModel.get().getPassword());
	}
}