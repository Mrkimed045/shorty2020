package assecorijeka.shorty2.service;

import assecorijeka.shorty2.dao.facade.UserDaoFacade;
import assecorijeka.shorty2.service.model.UserServiceModel;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class MyUserDetailsServiceImplementationTest {

    @Mock
    private UserDaoFacade userDaoFacade;

    @InjectMocks
    private MyUserDetailsServiceImplementation userDetailsService;

    @Test
    void loadUserByUsername_userExists(){
        //arrange
        String userId = "marco";
        when(userDaoFacade.existsById(userId)).thenReturn(true);
        UserServiceModel existingUser = new UserServiceModel("marco", "password");
        when(userDaoFacade.findById(userId)).thenReturn(Optional.of(existingUser));

        //act
        UserDetails returnDetails = userDetailsService.loadUserByUsername(userId);

        //assert
        assertEquals(returnDetails.getUsername(), userId);
    }

    @Test
    void loadUserByUsername_userDoesNotExist(){
        //arrange
        String userId = "marco";
        when(userDaoFacade.existsById(userId)).thenReturn(false);

        //act
        Throwable exception =  assertThrows(UsernameNotFoundException.class, () -> userDetailsService.loadUserByUsername(userId));

        //assert
        assertEquals("user not found", exception.getMessage());

    }

}
