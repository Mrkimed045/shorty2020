package assecorijeka.shorty2.service;

import assecorijeka.shorty2.dao.facade.ShortUrlDaoFacade;
import assecorijeka.shorty2.service.model.ShortUrlServiceModel;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class StatisticsServiceTest {

    @Mock
    private ShortUrlDaoFacade shortUrlDaoFacade;

    @InjectMocks
    private StatisticsService statisticsService;

    @Test
    void showStatisticsForUser_WithUrlsInDatabase(){
        //arrange
        String userId = "marco";
        ArrayList<ShortUrlServiceModel> list = new ArrayList<>();
        list.add(new ShortUrlServiceModel("https://www.facebook.com/", 302, "marco", "abcdefg", 0));
        when(shortUrlDaoFacade.findByUserID(userId)).thenReturn(list);

        //act
        ArrayList<ShortUrlServiceModel> responseList = statisticsService.showStatisticsForUser(userId);

        //assert
        assertFalse(responseList.isEmpty());
    }

    @Test
    void showStatisticsForUser_WithNoUrlsInDatabase(){
        //arrange
        String userId = "marco";
        ArrayList<ShortUrlServiceModel> list = new ArrayList<>();
        when(shortUrlDaoFacade.findByUserID(userId)).thenReturn(list);

        //act
        ArrayList<ShortUrlServiceModel> responseList = statisticsService.showStatisticsForUser(userId);

        //assert
        assertTrue(responseList.isEmpty());
    }

}
