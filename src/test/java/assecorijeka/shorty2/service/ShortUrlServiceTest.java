package assecorijeka.shorty2.service;

import assecorijeka.shorty2.dao.facade.ShortUrlDaoFacade;
import assecorijeka.shorty2.service.model.ShortUrlServiceModel;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class ShortUrlServiceTest {

    @Mock
    private ShortUrlDaoFacade shortUrlDaoFacade;

    @InjectMocks
    private ShortUrlService shortUrlService;


    @Test
    void saveShortUrl_UrlAlreadyinDatabase(){
        //arrange
        ShortUrlServiceModel searchInput = new ShortUrlServiceModel("https://www.facebook.com/", 302, "marco", null, 0);
        ShortUrlServiceModel searchOutput = new ShortUrlServiceModel("https://www.facebook.com/", 302, "marco", "abcdefg", 0);
        when(shortUrlDaoFacade.findByUserIDAndUrl(searchInput.getUserID(), searchInput.getUrl())).thenReturn(Optional.of(searchOutput));
        /*
        * searchInput je shortUrl kojeg primamo od contolera
        * ako postoji u bazi već zapis sa istim url-om i userId-om, onda pronalazimo taj url i spremamo u searchOutput
        * sada u kodu imamo shortUrl = shortUrlDaoFacade.findByUserIDAndUrl(userId, url);
        * To nam govori da će naš searchInput sada pokazivati na searchOutput
        * To je bitno kad dolazimo do instrukcije shortUrlDaoFacade.save()
        * Jer sada govorimo da save() prima taj searchOutput i istoga vrača
        */
        when(shortUrlDaoFacade.save(Optional.of(searchOutput))).thenReturn(Optional.of(searchOutput));

        //act
        Optional<ShortUrlServiceModel> returnModel = shortUrlService.saveShortUrl(Optional.of(searchInput));

        //assert
        assertTrue(returnModel.isPresent());
        assertNotNull(returnModel.get().getShortUrl());

    }

    @Test
    void saveShortUrl_UrlNotInDatabase(){
        //arrange
        ShortUrlServiceModel searchInput = new ShortUrlServiceModel("https://www.facebook.com/", 302, "marco", null, 0);
        Optional<ShortUrlServiceModel> searchOutput = Optional.empty();
        when(shortUrlDaoFacade.findByUserIDAndUrl(searchInput.getUserID(), searchInput.getUrl())).thenReturn(searchOutput);
        /*
         * searchInput je shortUrl kojeg primamo od contolera
         * ako NE postoji u bazi već zapis sa istim url-om i userId-om, onda kreiramo novi shortUrl za searchInput
         * To je bitno kad dolazimo do instrukcije shortUrlDaoFacade.save()
         * Jer sada govorimo da save() prima taj searchInput i istoga vrača
         */
        when(shortUrlDaoFacade.save(Optional.of(searchInput))).thenReturn(Optional.of(searchInput));

        //act
        Optional<ShortUrlServiceModel> returnModel = shortUrlService.saveShortUrl(Optional.of(searchInput));

        //assert
        assertTrue(returnModel.isPresent());
        assertNotNull(returnModel.get().getShortUrl());
    }
}
