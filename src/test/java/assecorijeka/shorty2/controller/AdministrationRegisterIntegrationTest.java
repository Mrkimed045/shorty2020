package assecorijeka.shorty2.controller;

import assecorijeka.shorty2.Shorty2Application;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import springfox.documentation.spring.web.json.Json;
import static org.junit.jupiter.api.Assertions.*;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = Shorty2Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AdministrationRegisterIntegrationTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void saveUser() throws JSONException {
        //assert
        HttpHeaders headers = new HttpHeaders();
        Json body = new Json("{" + "\"accountId\": \"marco\"" + "}");
        HttpEntity<Json> entity = new HttpEntity<Json>(body, headers);

        //act
        ResponseEntity<String> response = restTemplate.exchange(
                HelperMethods.createURLWithPort(port, "/administration/register"),
                HttpMethod.POST, entity, String.class);

        //assert
        JSONObject jsonObject = new JSONObject(response.getBody());
        assertTrue((Boolean) jsonObject.get("success"));
        assertNotNull(jsonObject.get("password"));
    }

}