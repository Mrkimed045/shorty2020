package assecorijeka.shorty2.controller;

import assecorijeka.shorty2.Shorty2Application;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import springfox.documentation.spring.web.json.Json;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = Shorty2Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AdministrationStatisticsIntegrationTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    @Sql({"/SQL/user_table/test_user_drop.sql", "/SQL/user_table/test_user_schema.sql", "/SQL/user_table/test_user_data.sql",
            "/SQL/shortUrl_table/test_shortUrl_drop.sql", "/SQL/shortUrl_table/test_shortUrl_schema.sql", "/SQL/shortUrl_table/test_shortUrl_data.sql"})
    public void showStatisticsForUser() throws JSONException {
        //assert
        HttpHeaders headers = HelperMethods.createHeaders("marco", "marco");
        HttpEntity<Json> entity = new HttpEntity<Json>(null, headers);

        //act
        ResponseEntity<String> response = restTemplate.exchange(
                HelperMethods.createURLWithPort(port,"/administration/statistics"),
                HttpMethod.GET, entity, String.class);

        //assert
        JSONObject jsonObject = new JSONObject(response.getBody());
        String expected = "{" + "\"https://gitlab.com/Mrkimed045/shorty2020\": 0" + "}";
        JSONAssert.assertEquals(expected, jsonObject, false);

    }

}