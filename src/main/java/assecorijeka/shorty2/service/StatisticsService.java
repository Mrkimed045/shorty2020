package assecorijeka.shorty2.service;

import assecorijeka.shorty2.dao.facade.ShortUrlDaoFacade;
import assecorijeka.shorty2.service.model.ShortUrlServiceModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class StatisticsService {

    @Autowired
    private ShortUrlDaoFacade shortUrlDaoFacade;

    public ArrayList<ShortUrlServiceModel> showStatisticsForUser(String userId){
        return shortUrlDaoFacade.findByUserID(userId);
    }
}
