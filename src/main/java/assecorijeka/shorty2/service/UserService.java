package assecorijeka.shorty2.service;

import assecorijeka.shorty2.dao.facade.UserDaoFacade;
import assecorijeka.shorty2.service.model.UserServiceModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.Optional;

@Service
public class UserService {

    private static final int PASSWORD_LENGTH = 15;
    
    @Autowired
    private UserDaoFacade userDaoFacade;

    public Optional<UserServiceModel> saveUser(Optional<UserServiceModel> userServiceModel){
        Optional<UserServiceModel> returnModel = Optional.empty();
        if(userServiceModel.isPresent()){
            if(!userDaoFacade.existsById(userServiceModel.get().getAccountId())){
                String pass = createUniquePassword();
                userServiceModel.get().setPassword(pass);
                returnModel = userDaoFacade.save(userServiceModel);
            }
        }
        return returnModel;
    }

    private String createUniquePassword() {
        char[] possibleCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()-_=+[{]}|;,<.>/?".toCharArray();
        String pass = RandomStringUtils.random( PASSWORD_LENGTH, 0, possibleCharacters.length-1, false, false, possibleCharacters, new SecureRandom() );
        return pass;
    }
}
