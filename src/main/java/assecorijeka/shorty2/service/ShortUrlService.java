package assecorijeka.shorty2.service;

import assecorijeka.shorty2.dao.facade.ShortUrlDaoFacade;
import assecorijeka.shorty2.service.model.ShortUrlServiceModel;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.Optional;

@Service
public class ShortUrlService {

    private static final int SHORT_URL_LENGTH = 7;

    @Autowired
    private ShortUrlDaoFacade shortUrlDaoFacade;

    public Optional<ShortUrlServiceModel> saveShortUrl(Optional<ShortUrlServiceModel> shortUrl){

        if(shortUrl.isPresent()){
            String userId = shortUrl.get().getUserID();
            String url = shortUrl.get().getUrl();
            if(shortUrlDaoFacade.findByUserIDAndUrl(userId, url).isPresent()){
                shortUrl = shortUrlDaoFacade.findByUserIDAndUrl(userId, url);
            }else{
                shortUrl.get().setShortUrl(createUniqueShortUrl());
            }
        }
        return shortUrlDaoFacade.save(shortUrl);
    }


    private String createUniqueShortUrl() {
        char[] possibleCharacters = (new String("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789")).toCharArray();

        String newShortUrl = RandomStringUtils.random( SHORT_URL_LENGTH, 0, possibleCharacters.length-1, false, false, possibleCharacters, new SecureRandom() );
        while (shortUrlDaoFacade.existsById(newShortUrl)){
            newShortUrl = RandomStringUtils.random( SHORT_URL_LENGTH, 0, possibleCharacters.length-1, false, false, possibleCharacters, new SecureRandom() );
        }
        return newShortUrl;
    }

}
