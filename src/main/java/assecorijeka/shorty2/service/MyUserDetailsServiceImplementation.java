package assecorijeka.shorty2.service;

import assecorijeka.shorty2.dao.facade.UserDaoFacade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import static assecorijeka.shorty2.config.SecurityConfiguration.USER_ROLE;

@Service
public class MyUserDetailsServiceImplementation implements UserDetailsService {

    @Autowired
    private UserDaoFacade userDaoFacade;

    @Override
    public UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        if(userDaoFacade.existsById(userId)){
            return User.withUsername(userId).password(encoder.encode(userDaoFacade.findById(userId).get().getPassword())).roles(USER_ROLE).build();
        }
        else throw new UsernameNotFoundException("user not found");
    }
}
