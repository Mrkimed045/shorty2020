package assecorijeka.shorty2.service;

import assecorijeka.shorty2.dao.facade.ShortUrlDaoFacade;
import assecorijeka.shorty2.service.model.ShortUrlServiceModel;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.view.RedirectView;

import java.util.Optional;


@Service
public class RedirectionService{

    @Autowired
    private ShortUrlDaoFacade shortUrlDaoFacade;

    public RedirectView redirect(String shortUrl) throws ResponseStatusException{
        Optional<String> fullUrl = getFullUrl(shortUrl);
        if(!fullUrl.isPresent()){
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Page does not exist in database!"
            );
        }else{
            HttpStatus statusCode = setStatusCode(getRedirectType(shortUrl));
            RedirectView redirectView = new RedirectView(fullUrl.get());
            redirectView.setStatusCode(statusCode);
            increaseCounterForRedirection(shortUrl);
            return redirectView;
        }
    }

    private HttpStatus setStatusCode(int redirectType) {
        if(redirectType == 302){
            return HttpStatus.FOUND;
        }else if(redirectType == 301){
            return HttpStatus.MOVED_PERMANENTLY;
        }else{
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "RedirectType somehow declared wrong till the end!"
            );
        }
    }

    private Optional<String> getFullUrl(String shortUrl){
        Optional<String> fullUrl = Optional.empty();
        Optional<ShortUrlServiceModel> shortURL = shortUrlDaoFacade.findById(shortUrl);
        if(shortURL.isPresent()){
            fullUrl = Optional.ofNullable(shortURL.get().getUrl());
        }
        return fullUrl;
    }

    private int getRedirectType(String shortUrl){
        int statusCode = 302;
        Optional<ShortUrlServiceModel> shortURL = shortUrlDaoFacade.findById(shortUrl);
        if(shortURL.isPresent()){
            statusCode = shortURL.get().getRedirectType();
        }
        return statusCode;
    }

    private void increaseCounterForRedirection(String shortUrl){
        Optional<ShortUrlServiceModel> shortURL = shortUrlDaoFacade.findById(shortUrl);
        if(shortURL.isPresent()){
            shortURL.get().setCounter(shortURL.get().getCounter() + 1);
            shortUrlDaoFacade.save(shortURL);
        }
    }
}
