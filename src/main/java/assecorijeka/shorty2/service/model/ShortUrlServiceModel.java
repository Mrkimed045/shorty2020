package assecorijeka.shorty2.service.model;

public class ShortUrlServiceModel {

    private String url;
    private int redirectType;
    private String userID;
    private String shortUrl;
    private int counter;

    public ShortUrlServiceModel(String url, int redirectType, String userID, String shortUrl, int counter) {
        this.url = url;
        this.redirectType = redirectType;
        this.userID = userID;
        this.shortUrl = shortUrl;
        this.counter = counter;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getShortUrl() {
        return shortUrl;
    }

    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }

    public int getRedirectType() {
        return redirectType;
    }

    public void setRedirectType(int redirectType) {
        this.redirectType = redirectType;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }
}
