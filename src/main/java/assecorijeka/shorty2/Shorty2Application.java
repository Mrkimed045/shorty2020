package assecorijeka.shorty2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@SpringBootApplication
@EnableSwagger2
public class Shorty2Application {

    public static void main(String[] args) {
        SpringApplication.run(Shorty2Application.class, args);
    }

    @Bean
    public Docket swaggerConfiguration(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                    .paths(PathSelectors.any())
                    .apis(RequestHandlerSelectors.basePackage("assecorijeka.shorty2"))
                    .build()
                .apiInfo(apiDetails());

    }

    private ApiInfo apiDetails(){
        return new ApiInfo(
                "Shorty2020",
                "Simple URL shortener application",
                "1.0",
                "",
                new springfox.documentation.service.Contact("Marco Davide Prpić", "", "prpicmd@gmail.com"),
                "",
                "",
                Collections.emptyList());
    }
}