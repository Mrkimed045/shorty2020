package assecorijeka.shorty2.config;

import assecorijeka.shorty2.service.MyUserDetailsServiceImplementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    public static final String USER_ROLE = "USER";
    public static final String ADMIN_ROLE = "ADMIN";

    @Autowired
    private MyUserDetailsServiceImplementation userDetailsService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/h2-console/**").hasRole(ADMIN_ROLE)
                .antMatchers("/administration/register").permitAll()
                .antMatchers("/administration/short").hasAnyRole(USER_ROLE, ADMIN_ROLE)
                .antMatchers("/administration/statistics").hasAnyRole(USER_ROLE, ADMIN_ROLE)
                .antMatchers("/shorty/**").hasAnyRole(USER_ROLE, ADMIN_ROLE)
                .antMatchers("/swagger-ui**").permitAll()
                .and().httpBasic().and().csrf().disable();

        //zbog h2-console na netu
        http.headers().frameOptions().disable();
    }

    @Override
    public void configure(AuthenticationManagerBuilder builder) throws Exception {
        builder.userDetailsService(userDetailsService);
        builder.inMemoryAuthentication()
                .withUser("admin")
                .password(passwordEncoder().encode("admin"))
                .roles(ADMIN_ROLE);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
