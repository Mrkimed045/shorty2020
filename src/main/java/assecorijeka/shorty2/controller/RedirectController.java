package assecorijeka.shorty2.controller;


import assecorijeka.shorty2.service.RedirectionService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;


@RestController
@RequestMapping("/shorty/")
public class RedirectController {

    @Autowired
    RedirectionService redirectionService;

    @GetMapping(path = "/{shortUrl}")
    public RedirectView urlRedirection(@PathVariable("shortUrl") String shortUrl) {
        return redirectionService.redirect(shortUrl);
    }
}

