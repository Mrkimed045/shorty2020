package assecorijeka.shorty2.controller;

import assecorijeka.shorty2.controller.mapper.ShortUrlMapper;
import assecorijeka.shorty2.controller.mapper.UserMapper;
import assecorijeka.shorty2.controller.model.ShortUrlControllerModel;
import assecorijeka.shorty2.controller.model.UserControllerModel;
import assecorijeka.shorty2.service.ShortUrlService;
import assecorijeka.shorty2.service.StatisticsService;
import assecorijeka.shorty2.service.UserService;
import assecorijeka.shorty2.service.model.ShortUrlServiceModel;
import assecorijeka.shorty2.service.model.UserServiceModel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@RequestMapping("/administration")
@RestController
public class AdministrationController {

    @Autowired
    private UserService userService;

    @Autowired
    private ShortUrlService shortUrlService;

    @Autowired
    private StatisticsService statisticsService;


    @PostMapping(path = "/register")
    public Map<String, Object> saveUser(@NotBlank @Valid @RequestBody UserControllerModel userControllerModel){

        Optional<UserServiceModel> userServiceModel = Optional.empty();

        Optional<UserServiceModel> convertedModel = UserMapper.fromControllerToService(Optional.of(userControllerModel));
        if(convertedModel.isPresent()){
            userServiceModel = userService.saveUser(convertedModel);
        }

        Optional<UserControllerModel> userControllerModelReturned = Optional.empty();

        if(userServiceModel.isPresent()){
            userControllerModelReturned = UserMapper.fromServiceToController(userServiceModel);
        }

        return createRegisterResponseMap(userControllerModelReturned);
    }


    @PostMapping(path = "/short")
    public Map<String, Object> createShorterUrl(@NotBlank @Valid @RequestBody ShortUrlControllerModel shortUrl){

        Optional<ShortUrlServiceModel> shortUrlServiceModel = Optional.empty();

        Optional<String> userId = Optional.ofNullable(getCurrentUserId());
        if(userId.isPresent()){
            Optional<ShortUrlServiceModel> convertedModel = ShortUrlMapper.fromControllerToService(Optional.of(shortUrl));
            if(convertedModel.isPresent()){
                convertedModel.get().setUserID(userId.get());
                shortUrlServiceModel = shortUrlService.saveShortUrl(convertedModel);
            }
        }

        Optional<ShortUrlControllerModel> shortUrlControllerModel = Optional.empty();

        if(shortUrlServiceModel.isPresent()){
            shortUrlControllerModel = ShortUrlMapper.fromServiceToController(shortUrlServiceModel);
        }

        return createShortUrlResponseMap(shortUrlControllerModel);
    }


    @GetMapping(path = "/statistics")
    public Map<String, Object> showStatisticsForUser(){
        Optional<ArrayList<ShortUrlControllerModel>> controllerModelList = Optional.empty();

        Optional<String> userId = Optional.ofNullable(getCurrentUserId());
        if(userId.isPresent()){
            Optional<ArrayList<ShortUrlServiceModel>> serviceList = Optional.ofNullable(statisticsService.showStatisticsForUser(userId.get()));
            if(serviceList.isPresent()){
                controllerModelList = Optional.of(ShortUrlMapper.fromServiceListToControllerList(serviceList));
            }
        }else{
            return createAuthFailureResponse();
        }

        return createStatisticsResponseMap(controllerModelList);
    }


    private String getCurrentUserId(){
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return user.getUsername();
    }


    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }


    private HashMap<String, Object> createRegisterResponseMap(Optional<UserControllerModel> userControllerModel) {
        HashMap<String, Object> response = new HashMap<>();
        if(userControllerModel.isPresent()){
            response.put("success", true);
            response.put("password", userControllerModel.get().getPassword());
        }else{
            response.put("success", false);
            response.put("description", "Account ID already exists!");

        }
        return response;
    }

    private HashMap<String, Object> createShortUrlResponseMap(Optional<ShortUrlControllerModel> shortUrlControllerModel) {
        HashMap<String, Object> response = new HashMap<>();

        if(shortUrlControllerModel.isPresent()){
            response.put("shortUrl", "http://localhost:8080/shorty/" + shortUrlControllerModel.get().getShortUrl());
        }else{
            response = createAuthFailureResponse();
        }

        return response;
    }

    private Map<String, Object> createStatisticsResponseMap(Optional<ArrayList<ShortUrlControllerModel>> controllerModelList) {
        HashMap<String, Object> response = new HashMap<>();
        if(controllerModelList.isPresent()){
            for(ShortUrlControllerModel controllerModel : controllerModelList.get()){
                response.put(controllerModel.getUrl(), controllerModel.getCounter());
            }
        }
        return response;
    }

    private HashMap<String, Object> createAuthFailureResponse() {
        HashMap<String, Object> response = new HashMap<>();
        response.put("description", "User authentication failed!");
        return response;
    }

}
