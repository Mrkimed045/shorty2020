package assecorijeka.shorty2.controller.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;


public class UserControllerModel {

	@NotBlank(message = "BAD REQUEST: accountId must be in request body! BAD REQUEST!")
	@JsonProperty("accountId")
	private String accountId;

	@JsonIgnore
	private String password;
	
	public UserControllerModel(String accountId) {
		this.accountId = accountId;
		this.password = null;
	}
	
	public UserControllerModel( String accountId, String password) {
		this.accountId = accountId;
		this.password = password;
	}
	
	public String getAccountId() {
		return accountId;
	}
	
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
}