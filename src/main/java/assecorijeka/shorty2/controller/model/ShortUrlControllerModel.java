package assecorijeka.shorty2.controller.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;


public class ShortUrlControllerModel {

    @NotBlank(message = "BAD REQUEST: url must be in requestbody!")
    @JsonProperty("url")
    private String url;

    @Range(min=301, max=302, message = "BAD REQUEST: redirectType has to be 301 or 302!")
    @JsonProperty("redirectType")
    private int redirectType;

    @JsonIgnore
    private String userID;
    @JsonIgnore
    private String shortUrl;
    @JsonIgnore
    private int counter;

    public ShortUrlControllerModel() {
        this.redirectType = 302;
        this.counter = 0;
    }

    public ShortUrlControllerModel(String url, int redirectType, String userID, String shortUrl, int counter) {
        this.url = url;
        this.redirectType = redirectType;
        this.userID = userID;
        this.shortUrl = shortUrl;
        this.counter = counter;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getShortUrl() {
        return shortUrl;
    }

    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }

    public int getRedirectType() {
        return redirectType;
    }

    public void setRedirectType(int redirectType) {
        this.redirectType = redirectType;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }
}
