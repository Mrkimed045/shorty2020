package assecorijeka.shorty2.controller.mapper;

import assecorijeka.shorty2.controller.model.UserControllerModel;
import assecorijeka.shorty2.service.model.UserServiceModel;

import java.util.Optional;

public class UserMapper {
	
	public static Optional<UserServiceModel> fromControllerToService(Optional<UserControllerModel> userControllerModel) {
		Optional<UserServiceModel> returnModel = Optional.empty();
		if(userControllerModel.isPresent()){
			returnModel = Optional.of(new UserServiceModel(userControllerModel.get().getAccountId(), userControllerModel.get().getPassword()));
		}
		return returnModel;
	}

	public static Optional<UserControllerModel> fromServiceToController(Optional<UserServiceModel> userServiceModel) {
		Optional<UserControllerModel> returnModel = Optional.empty();
		if(userServiceModel.isPresent()){
			returnModel = Optional.of(new UserControllerModel(userServiceModel.get().getAccountId(), userServiceModel.get().getPassword()));
		}
		return returnModel;
	}
}
