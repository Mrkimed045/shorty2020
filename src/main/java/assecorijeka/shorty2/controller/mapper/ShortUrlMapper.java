package assecorijeka.shorty2.controller.mapper;

import assecorijeka.shorty2.controller.model.ShortUrlControllerModel;
import assecorijeka.shorty2.service.model.ShortUrlServiceModel;

import java.util.ArrayList;
import java.util.Optional;

public class ShortUrlMapper {
    public static Optional<ShortUrlServiceModel> fromControllerToService(Optional<ShortUrlControllerModel> shortUrlControllerModel) {
        Optional<ShortUrlServiceModel> returnModel = Optional.empty();
        if(shortUrlControllerModel.isPresent()){
            returnModel = Optional.of(new ShortUrlServiceModel(shortUrlControllerModel.get().getUrl(),
                                                shortUrlControllerModel.get().getRedirectType(),
                                                shortUrlControllerModel.get().getUserID(),
                                                shortUrlControllerModel.get().getShortUrl(),
                                                shortUrlControllerModel.get().getCounter()));
        }
        return returnModel;

    }

    public static Optional<ShortUrlControllerModel> fromServiceToController(Optional<ShortUrlServiceModel> shortUrlServiceModel) {
        Optional<ShortUrlControllerModel> returnModel = Optional.empty();
        if(shortUrlServiceModel.isPresent()){
            returnModel = Optional.of(new ShortUrlControllerModel(shortUrlServiceModel.get().getUrl(),
                                                shortUrlServiceModel.get().getRedirectType(),
                                                shortUrlServiceModel.get().getUserID(),
                                                shortUrlServiceModel.get().getShortUrl(),
                                                shortUrlServiceModel.get().getCounter()));
        }
        return returnModel;

    }

    public static ArrayList<ShortUrlControllerModel> fromServiceListToControllerList(Optional<ArrayList<ShortUrlServiceModel>> serviceModelList){
        ArrayList<ShortUrlControllerModel> controllerModelList = new ArrayList<>();
        if(serviceModelList.isPresent()){
            for(ShortUrlServiceModel serviceModel : serviceModelList.get()){
                Optional<ShortUrlControllerModel> convertedModel = fromServiceToController(Optional.of(serviceModel));
                if(convertedModel.isPresent()){
                    ShortUrlControllerModel addableModel = convertedModel.get();
                    controllerModelList.add(addableModel);
                }
            }
        }
        return controllerModelList;
    }
}
