package assecorijeka.shorty2.dao.model;

import org.hibernate.validator.constraints.Range;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;

@Entity
public class ShortUrlDaoModel {

    @NotBlank(message = "INSERTION ERROR: ShortUrl can't be saved in database if url is empty or null!")
    private String url;
    @Range(min=301, max=302,message = "INSERTION ERROR: ShortUrl can't be saved in database if redirectType is not 301 or 302!")
    private int redirectType;
    @NotBlank(message = "INSERTION ERROR: ShortUrl can't be saved in database if userId is empty or null!")
    private String userID;
    @Id
    @NotBlank(message = "INSERTION ERROR: ShortUrl can't be saved in database if url is empty or null!")
    private String shortUrl;
    @PositiveOrZero
    private int counter;

    public ShortUrlDaoModel() {
    }

    public ShortUrlDaoModel(String url, int redirectType, String userID, String shortUrl, int counter) {
        this.url = url;
        this.redirectType = redirectType;
        this.userID = userID;
        this.shortUrl = shortUrl;
        this.counter = counter;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getShortUrl() {
        return shortUrl;
    }

    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }

    public int getRedirectType() {
        return redirectType;
    }

    public void setRedirectType(int redirectType) {
        this.redirectType = redirectType;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }
}
