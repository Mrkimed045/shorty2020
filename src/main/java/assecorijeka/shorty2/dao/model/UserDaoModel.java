package assecorijeka.shorty2.dao.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

@Entity
public class UserDaoModel {
    @Id
    @NotBlank(message = "INSERTION ERROR: User can't be saved in database if accountId is empty or null!")
    private String accountId;
    @NotBlank(message = "INSERTION ERROR: User can't be saved in database if password is empty or null!")
    private String password;

    public UserDaoModel() {
    }

    public UserDaoModel(String accountId, String password) {
        this.accountId = accountId;
        this.password = password;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}