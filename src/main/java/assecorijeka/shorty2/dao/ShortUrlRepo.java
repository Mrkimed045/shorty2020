package assecorijeka.shorty2.dao;

import assecorijeka.shorty2.dao.model.ShortUrlDaoModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ShortUrlRepo extends JpaRepository<ShortUrlDaoModel, String> {
    Optional<List<ShortUrlDaoModel>> findByUserID(String userId);
    Optional<ShortUrlDaoModel> findByUserIDAndUrl(String userId, String url);
}
