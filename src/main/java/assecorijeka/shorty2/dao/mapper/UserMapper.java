package assecorijeka.shorty2.dao.mapper;

import assecorijeka.shorty2.dao.model.UserDaoModel;
import assecorijeka.shorty2.service.model.UserServiceModel;

import java.util.Optional;

public class UserMapper {

	public static Optional<UserDaoModel> fromServiceToDao(Optional<UserServiceModel> userServiceModel) {
		Optional<UserDaoModel> returnModel = Optional.empty();
		if(userServiceModel.isPresent()){
			returnModel = Optional.of(new UserDaoModel(userServiceModel.get().getAccountId(), userServiceModel.get().getPassword()));
		}
		return returnModel;
	}

	public static Optional<UserServiceModel> fromDaoToService(Optional<UserDaoModel> userDaoModel) {
		Optional<UserServiceModel> returnModel = Optional.empty();
		if(userDaoModel.isPresent()){
			returnModel = Optional.of(new UserServiceModel(userDaoModel.get().getAccountId(), userDaoModel.get().getPassword()));
		}
		return returnModel;
	}
}
