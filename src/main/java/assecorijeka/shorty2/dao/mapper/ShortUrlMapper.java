package assecorijeka.shorty2.dao.mapper;

import assecorijeka.shorty2.dao.model.ShortUrlDaoModel;
import assecorijeka.shorty2.service.model.ShortUrlServiceModel;

import java.util.Optional;

public class ShortUrlMapper {

    public static Optional<ShortUrlDaoModel> fromServiceToDao(Optional<ShortUrlServiceModel> shortUrlServiceModel) {
        Optional<ShortUrlDaoModel> returnModel = Optional.empty();
        if(shortUrlServiceModel.isPresent()){
            returnModel = Optional.of(new ShortUrlDaoModel(shortUrlServiceModel.get().getUrl(),
                    shortUrlServiceModel.get().getRedirectType(),
                    shortUrlServiceModel.get().getUserID(),
                    shortUrlServiceModel.get().getShortUrl(),
                    shortUrlServiceModel.get().getCounter()));
        }
        return returnModel;
    }

    public static Optional<ShortUrlServiceModel> fromDaoToService(Optional<ShortUrlDaoModel> shortUrlDaoModel) {
        Optional<ShortUrlServiceModel> returnModel = Optional.empty();
        if(shortUrlDaoModel.isPresent()){
            returnModel = Optional.of(new ShortUrlServiceModel(shortUrlDaoModel.get().getUrl(),
                    shortUrlDaoModel.get().getRedirectType(),
                    shortUrlDaoModel.get().getUserID(),
                    shortUrlDaoModel.get().getShortUrl(),
                    shortUrlDaoModel.get().getCounter()));
        }
        return returnModel;
    }
}
