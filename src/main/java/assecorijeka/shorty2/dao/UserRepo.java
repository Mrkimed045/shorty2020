package assecorijeka.shorty2.dao;

import assecorijeka.shorty2.dao.model.UserDaoModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepo extends JpaRepository<UserDaoModel, String> {
}
