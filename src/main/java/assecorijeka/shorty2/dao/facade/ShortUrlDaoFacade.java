package assecorijeka.shorty2.dao.facade;

import assecorijeka.shorty2.dao.ShortUrlRepo;
import assecorijeka.shorty2.dao.mapper.ShortUrlMapper;
import assecorijeka.shorty2.dao.model.ShortUrlDaoModel;
import assecorijeka.shorty2.service.model.ShortUrlServiceModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ShortUrlDaoFacade {

    @Autowired
    private ShortUrlRepo shortUrlRepo;

    public Optional<ShortUrlServiceModel> save(Optional<ShortUrlServiceModel> shortUrlServiceModel){
        Optional<ShortUrlDaoModel> shortUrlDaoModel = Optional.empty();
        if(shortUrlServiceModel.isPresent()){
            Optional<ShortUrlDaoModel> convertedModel = ShortUrlMapper.fromServiceToDao(shortUrlServiceModel);
            if(convertedModel.isPresent()){
                shortUrlDaoModel = Optional.of(shortUrlRepo.save(convertedModel.get()));
            }
        }
        return ShortUrlMapper.fromDaoToService(shortUrlDaoModel);
    }

    public boolean existsById(String id){
        return shortUrlRepo.existsById(id);
    }

    public Optional<ShortUrlServiceModel> findById(String id){
        Optional<ShortUrlServiceModel> returnModel = Optional.empty();
        if(shortUrlRepo.existsById(id)) {
            returnModel = ShortUrlMapper.fromDaoToService(shortUrlRepo.findById(id));
        }
        return returnModel;
    }

    public Optional<ShortUrlServiceModel> findByUserIDAndUrl(String userId, String url){
        Optional<ShortUrlServiceModel> returnModel = Optional.empty();
        if(shortUrlRepo.findByUserIDAndUrl(userId, url).isPresent()){
            returnModel = ShortUrlMapper.fromDaoToService(shortUrlRepo.findByUserIDAndUrl(userId, url));
        }
        return returnModel;
    }

    public ArrayList<ShortUrlServiceModel> findByUserID(String userId){
        ArrayList<ShortUrlServiceModel> returnList = new ArrayList<>();
        if(shortUrlRepo.findByUserID(userId).isPresent()){
            Optional<List<ShortUrlDaoModel>> daoList = shortUrlRepo.findByUserID(userId);
            if(daoList.isPresent()){
                for(ShortUrlDaoModel daoModel : daoList.get()){
                    Optional<ShortUrlServiceModel> convertedModel = ShortUrlMapper.fromDaoToService(Optional.ofNullable(daoModel));
                    convertedModel.ifPresent(returnList::add);
                }
            }
        }
        return returnList;
    }
}
