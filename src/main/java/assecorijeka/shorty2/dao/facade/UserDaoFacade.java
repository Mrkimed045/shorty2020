package assecorijeka.shorty2.dao.facade;

import assecorijeka.shorty2.dao.UserRepo;
import assecorijeka.shorty2.dao.mapper.UserMapper;
import assecorijeka.shorty2.dao.model.UserDaoModel;
import assecorijeka.shorty2.service.model.UserServiceModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserDaoFacade {
	
	@Autowired
	private UserRepo userRepo;
	
	public Optional<UserServiceModel> save(Optional<UserServiceModel> userServiceModel) {
		Optional<UserDaoModel> userDaoModel = Optional.empty();
		if(userServiceModel.isPresent()){
			Optional<UserDaoModel> convertedModel = UserMapper.fromServiceToDao(userServiceModel);
			if(convertedModel.isPresent()){
				userDaoModel = Optional.of(userRepo.save(convertedModel.get()));
			}
		}
		return UserMapper.fromDaoToService(userDaoModel);
	}

	public boolean existsById(String userId){
		return userRepo.existsById(userId);
	}

	public Optional<UserServiceModel> findById(String userId){
		Optional<UserServiceModel> returnModel = Optional.empty();
		if(userRepo.existsById(userId)){
			returnModel = UserMapper.fromDaoToService(userRepo.findById(userId));
		}
		return returnModel;
	}
}
